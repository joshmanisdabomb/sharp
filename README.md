# Sharp - A Fully-Featured ComputerCraft OS #

Sharp is a large-scale operating system written in LUA for the ComputerCraft Minecraft mod. It plans to give many programs out-of-the-box, and a development kit to create your own. It is styled like Windows 8 and Android.

## Planned Features ##

* Installer
* Multiple Users at Once
* Multiple Apps
* Permissions for each user and app. (Sharp apps need permissions to make big changes)
* Hard Drive (Computers) and Floppy Disk Management
* Notification Bar (and Notifications)
* App Splitscreen
* Customisable Home Menu
* Help app
* Users can make their own apps

### Apps ###
#### On Full Release: ####

* File Browser (can network with other Sharp computers through wireless or wired modem)
* Instant Chat (with other Sharp computers, peer to peer or on chat servers)
* Text Editor
* Development Kit
* Document Writer
* Spreadsheet Creator
* Presentation Creator
* Internet Browser
* Image Viewer (with extended character support)
* Image Editor (with extended character support)
* Music Player (through Command Block support)
* Music Composer (through Command Block support)
* Some games

#### After Release ####
* Screen Recorder
* Video Player
* Video Editor
* More games

#### Maybes ####
* App Store