---------- Instance Internal API

sh.instances = {}

sh.instances.all = {}
sh.instances.current = nil

---------- Instance Object

sh.instance = {}

function sh.instance:constructor(user, app, open)
  open = open or 0

  table.insert(sh.instances.all, self)
  self.id = #sh.instances.all

  self.user = user
  if type(user) == "string" then
    self.user = sh.users.all[user]
    if not self.user then
      error("User " .. user .. " is not a loaded user.", 2)
    end
  end

  self.app = app
  if type(app) == "string" then
    self.app = sh.apps.all[app]
    if not self.app then
      error("App " .. app .. " is not a loaded application.", 2)
    end
  end
  
  self.layer = sh.instances.system
  if self.user then
    self.layer = self.user.layer
  end

  self.routine = {}
  for k,v in pairs(self.app.routine) do
    self.routine[k] = self:buildRoutine(v)
  end

  self.canvas = self.canvas or sh.graphics.canvas({}, sh.graphics.main, 1, 2, sh.graphics.width, sh.graphics.height - 2, false)

  if open >= 1 then
    self.layer:start(self, { foreground = open >= 2, focus = open >= 3 and 1 or #self.entries })
  end

  return self
end

function sh.instance._get(obj, index)
  if index == 'x' or index == 'y' or index == 'w' or index == 'width' or index == 'h' or index == 'height' or index == 'x1' or index == 'x2' or index == 'y1' or index == 'y2' or index == 'sx1' or index == 'sx2' or index == 'sy1' or index == 'sy2' then
    return true, obj.canvas[index]
  end
end

function sh.instance:buildRoutine(routine)
  return function (...)
    self:prepare()
    local result = { routine(self, ...) }
    self:teardown()
    return table.unpack(result)
  end
end

function sh.instance:prepare()
  sh.instances.current = self
  term.redirect(self.canvas.term)
end

function sh.instance:teardown()
  sh.instances.current = nil
  term.redirect(term.native())
end

sh.instance = sh.class.new(sh.instance, 'Instance')

---------- Layer Object

sh.instances.layer = {}

function sh.instances.layer:constructor()
  self.entries = {}
end

function sh.instances.layer:start(instance, options)
  self.entries[instance.id] = options or {}
  self.entries[instance.id].instance = instance
  sh.err.handler(instance.routine.start, sh.err.appRun, instance, 'start')
  if self.entries[instance.id].foreground then
    sh.err.handler(instance.routine.open, sh.err.appRun, instance, 'open')
  end
  self:focus(instance, self.entries[instance.id].focus or 1)
  return self.entries[instance.id]
end

function sh.instances.layer:restore(instance)
  if not self.entries[instance.id] then
    self:start(instance)
  end
  if not self.entries[instance.id].foreground then
    self.entries[instance.id].foreground = true
    sh.err.handler(instance.routine.open, sh.err.appRun, instance, 'open')
  end
  return self.entries[instance.id]
end

function sh.instances.layer:minimise(instance)
  if not self.entries[instance.id] then
    self:start(instance)
  end
  if self.entries[instance.id].foreground then
    self.entries[instance.id].foreground = false
    sh.err.handler(instance.routine.close, sh.err.appRun, instance, 'close')
  end
  return self.entries[instance.id]
end

function sh.instances.layer:destroy(instance)
  local entry = self:terminate(instance)
  sh.err.handler(instance.routine.destroy, sh.err.appRun, instance, 'destroy')
  return entry
end

function sh.instances.layer:terminate(instance)
  local entry = self.entries[instance.id]
  self.entries[instance.id] = nil
  return entry
end

function sh.instances.layer:focus(instance, focus)
  self.entries[instance.id].focus = focus
  if focus == 1 then
    sh.err.handler(instance.routine.focus, sh.err.appRun, instance, 'focus')
  end
  for k,v,i in sh.table.pairsSorted(self.entries, function(a, b) return a.focus < b.focus end) do
    if k ~= instance.id then
      if focus == i and i == 2 then
        sh.err.handler(instance.routine.blur, sh.err.appRun, instance, 'blur')
      end
      if i >= focus then
        i = i + 1
      end
      v.focus = i
    end
  end
end

sh.instances.layer = sh.class.new(sh.instances.layer, 'Layer')