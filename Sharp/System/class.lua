sh.class = {}

function sh.class.new(mt, name, base)
  local class = sh.table.clone(mt)

  class.base = base
  class.ascendants = { class.base, table.unpack(class.base and class.base.ascendants or {}) }

  local _opti_string_sub = string.sub
  local _opti_sh_table_contains = sh.table.contains
  local _opti_sh_string_capitalise = sh.string.capitalise

  class.__index = function(obj, index)
    local prefix = _opti_string_sub(index, 1, 1)
    
    if prefix ~= '_' then
      local direct = obj["_direct"]
      if type(direct) == 'function' then
        direct = direct(obj, index)
      elseif type(direct) == 'table' then
        direct = _opti_sh_table_contains(direct, index)
      end
      if direct then
        local func = rawget(obj, index) or mt[index]
        if func then
          return function(...) return func(obj, ...) end
        end
      end
    end

    local meta = mt[index]
    if meta ~= nil then return meta end

    if prefix ~= '_' then
      local getter = obj["_get" .. _opti_sh_string_capitalise(index)]
      if type(getter) == 'function' then return getter(obj, index) end

      local getter2 = obj["_get"]
      if type(getter2) == 'function' then
        local ret = { getter2(obj, index) }
        if ret[1] then
          return unpack(ret, 2)
        end
      end
    end

    if class.base then
      local parent = class.base[index]
      if parent ~= nil then return parent end
    end

    return rawget(obj, index)
  end
  class.__newindex = function(obj, index, value)
    if _opti_string_sub(index, 1, 1) ~= '_' then
      local setter = obj["_set" .. _opti_sh_string_capitalise(index)]
      if type(setter) == 'function' then return setter(obj, index, value) end

      local setter2 = obj["_set"]
      if type(setter2) == 'function' then
        local ret = { setter2(obj, index) }
        if ret[1] then
          return unpack(ret, 2)
        end
      end
    end

    return rawset(obj, index, value)
  end

  class.__name = name

  class.constructor = mt.constructor or function() end

  class.shclass = true
  class.template = mt
  class.meta = {}

  function class.meta:__call(obj, ...)
    obj = obj or {}
    setmetatable(obj, class)

    local constructs = sh.table.reverse({ class, table.unpack(class.ascendants) })
    for k,v in ipairs(constructs) do
      v.constructor(obj, ...)
    end

    return obj
  end

  setmetatable(class, class.meta)

  return class
end

function sh.class.singleton(mt, name, base)
  local class = sh.class.new(mt, name, base)
  local instance = class({})

  setmetatable(class, nil)
  class.meta = nil

  return instance, class
end

function sh.class.get(obj)
  local mt = getmetatable(obj)
  if mt and mt.shclass then
    return mt
  end
  return nil
end

function sh.class.of(obj, class)
  if sh.class.is(obj, class) then
    return true
  end

  local compare = sh.class.get(obj)
  if not compare then
    return
  end

  for k,v in pairs(compare.ascendants) do
    if v == class then
      return true
    end
  end
  return false
end

function sh.class.is(obj, class)
  local compare = sh.class.get(obj)
  if not compare then
    return
  end

  return compare == class
end