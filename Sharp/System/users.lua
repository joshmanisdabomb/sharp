---------- Users Internal API

sh.users = {}

sh.users.all = {}
sh.users.current = nil
sh.users.active = {}

function sh.users.loadAll()
  local all = {}
  for k,v in pairs(fs.list(sh.path.users)) do
    all[v] = sh.user({ path = fs.combine(sh.path.users, v) })
  end
  return all
end

sh.boot.log("Created load all users function.", colors.green)
  
---------- Users Object

sh.user = {}

function sh.user:constructor()
  self.name = fs.getName(self.path)
  self.active = false

  self.home = sh.config['apps.home']
  self.layer = sh.instances.layer()
  
  --Loading groups this user is in.
  local groups = fs.open(fs.combine(self.path, "groups.dat"), "r")
  self.groups = textutils.unserialise(groups.readAll())
  groups.close()
  
  --Loading extra permissions.
  local perms = fs.open(fs.combine(self.path, "permissions.dat"), "r")
  self.perms = textutils.unserialise(perms.readAll())
  perms.close()
end

sh.user = sh.class.new(sh.user, 'User')

sh.boot.log("Created user class.", colors.green)