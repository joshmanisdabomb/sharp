sh.string = {}

local module = require("cc.strings")
for k,v in pairs(module) do
  sh.string[k] = v
end

function sh.string.capitalise(str)
  return (str:gsub("^%l", string.upper))
end

function sh.string.lpad(str, char, len)
  return string.rep(char, len - string.len(str)) .. str
end

function sh.string.rpad(str, char, len)
  return str .. string.rep(char, len - string.len(str))
end

function sh.string.ltrim(str)
  return (string.gsub(str, "^%s*(.-)$", "%1"))
end

function sh.string.rtrim(str)
  return (string.gsub(str, "^(.-)%s*$", "%1"))
end

function sh.string.rtrim(str)
  return sh.string.rtrim(sh.string.ltrim(str))
end

function sh.string.truncate(str, len, terminator)
  if string.len(str) <= len then
    return str
  end

  terminator = terminator or '...'

  return string.sub(str, 1, len - string.len(terminator)) .. terminator
end

function sh.string.replace(str, replacement, start, length)
  if start < 0 then start = #str + start + 1 end
  if not length then length = string.len(replacement) end

  if start < 1 then start = 1 end
  if start > #str then start = #str + 1 end

  if start + length - 1 > #str then length = #str - start + 1 end

  local before = string.sub(str, 1, start - 1)
  local after = string.sub(str, start + length)
  return table.concat({before, replacement, after})
end

function sh.string.mask(mask, ...)
  local strings = {...}
  local result = {}
  local w = string.len(mask)
  for i=1,w,1 do
    local index = tonumber(string.sub(mask, i, i), 36)
    table.insert(result, string.sub(strings[index], i, i))
  end
  return table.concat(result)
end