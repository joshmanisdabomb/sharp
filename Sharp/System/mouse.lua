---------- Mouse Internal API

sh.mouse = { current = {}, known = {}, prev = {}, relative = {}, first = {}, offset = {} }

function sh.mouse.handle(event)
  if event[1] == "mouse_click" then sh.mouse.pressed(table.unpack(event, 2))
  elseif event[1] == "mouse_drag" then sh.mouse.dragged(table.unpack(event, 2))
  elseif event[1] == "mouse_scroll" then sh.mouse.scrolled(table.unpack(event, 2))
  elseif event[1] == "mouse_up" then sh.mouse.released(table.unpack(event, 2)) end
end

function sh.mouse.pressed(button, x, y)
  sh.mouse.current = sh.mouse.current or {}
  sh.mouse.current.buttons = sh.mouse.current.buttons or {}
  sh.mouse.current.buttons[button] = true
  sh.mouse.current.x = x
  sh.mouse.current.y = y
  sh.mouse.known = sh.mouse.current

  sh.mouse.first = sh.table.clone(sh.mouse.current)
  sh.mouse.prev = {}
  sh.mouse.relative = { x = 0, y = 0 }
  sh.mouse.offset = { x = 0, y = 0 }
end

function sh.mouse.dragged(button, x, y)
  sh.mouse.prev = { x = sh.mouse.current.x, y = sh.mouse.current.y }
  sh.mouse.relative = { x = x - sh.mouse.current.x, y = y - sh.mouse.current.y }
  sh.mouse.offset = { x = x - sh.mouse.first.x, y = y - sh.mouse.first.y }

  sh.mouse.current.buttons = sh.mouse.current.buttons or {}
  sh.mouse.current.buttons[button] = true
  sh.mouse.current.x = x
  sh.mouse.current.y = y
  sh.mouse.known = sh.mouse.current
end

function sh.mouse.scrolled(scroll, x, y)
  sh.mouse.known = sh.mouse.known or {}
  sh.mouse.known.x = x
  sh.mouse.known.y = y

  if sh.mouse.current.x or sh.mouse.current.y then
    sh.mouse.relative = { x = x - sh.mouse.current.x, y = y - sh.mouse.current.y }
    sh.mouse.offset = { x = x - sh.mouse.first.x, y = y - sh.mouse.first.y }
  end
end

function sh.mouse.released(button, x, y)
  sh.mouse.current.buttons[button] = false
  if #sh.table.keys(sh.mouse.current.buttons) <= 0 then
    sh.mouse.current = {}
  end
  sh.mouse.known.x = x
  sh.mouse.known.y = y

  sh.mouse.relative = { x = x - sh.mouse.known.x, y = y - sh.mouse.known.y }
  sh.mouse.offset = { x = x - sh.mouse.first.x, y = y - sh.mouse.first.y }
end