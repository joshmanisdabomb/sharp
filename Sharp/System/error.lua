sh.err = {}

local module = require("cc.internal.syntax.parser")
sh.err.parser = module.parser

function sh.err.handler(call, handle, ...)
  local extra = {...}
  local result = { xpcall(call, function(err)
    local trace = debug.traceback(err, 2)
    return { err, trace }
  end) }
  if not result[1] then
    handle(result[2][1], result[2][2], table.unpack(extra))
  end
  return table.unpack(result)
end

function sh.err.screen(text, trace, header, bg, keys)
  if sh.routine then
    sh.routine.pause = true
  end

  sh.log.runtime.write(header .. ": " .. tostring(trace), colors.red, 2)

  term.redirect(term.native())

  local blink = term.getCursorBlink()
  term.setCursorBlink(false)

  local lines = sh.string.wrap(text, math.max(sh.graphics.width - 2, 1))

  term.setBackgroundColor(bg)
  term.setTextColor(colors.white)
  term.clear()

  local y = math.ceil((sh.graphics.height / 2) - math.ceil(#lines/2))
  for k,v in pairs(lines) do
    term.setCursorPos(2, y + k)
    term.write(v)
  end
  
  sh.graphics.setCursorY(1)
  term.setBackgroundColor(colors.white)
  term.clearLine()
  term.setTextColor(bg)
  sh.graphics.writeCenter("- " .. header .. " -", true)

  local key = nil
  while key == nil do
    local event = { os.pullEventRaw() }
    if event[1] == 'key' then
      for k,v in pairs(keys) do
        if event[2] == v then
          key = v
          break
        end
      end
    end
  end

  term.setCursorBlink(blink)
  term.setBackgroundColor(colors.black)
  term.setTextColor(colors.white)
  term.setCursorPos(1,1)
  term.clear()

  if sh.routine then
    sh.routine.pause = false
  end

  return key
end

function sh.err.fatal(err, trace)
  sh.err.screen(sh.about.name .. " encountered a fatal error and must now terminate. Any unsaved progress in all open applications will be lost.\n\nBelow is the stack trace that caused this error:\n\n" .. tostring(err) .. "\n\nPressing Enter will return to CraftOS. You can also hold Ctrl+R to restart your computer.", trace, "Fatal Error", colors.red, {keys.enter})
  error(trace, 0)
end

function sh.err.appRun(err, trace, instance, routine)
  local key = sh.err.screen("The " .. sh.about.name .. " application " .. instance.app.longname .. " has encountered an error in its routine function '" .. routine .. "'.\n\nBelow is the stack trace that caused this error:\n\n" .. tostring(err) .. "\n\nYou can press Enter to attempt to continue running the application, or you can press Backspace to terminate the application and lose any unsaved progress.\n\nYou can also hold Ctrl+R to restart your computer.", trace, "Application Error", colors.blue, {keys.enter, keys.backspace})
  if key == keys.backspace then
    instance.layer:terminate(instance)
  end
end

function sh.err.appLoad(err, trace, instance)
  sh.err.screen(sh.about.name .. " encountered an error loading the application " .. instance.app.longname .. "\n\nBelow is the stack trace that caused this error:\n\n" .. tostring(err) .. "\n\nPressing Enter will continue loading " .. sh.about.name .. " without this application.\n\nYou can also hold Ctrl+R to restart your computer.", trace,"Application Error", colors.blue, {keys.enter})
end