---------- UI Internal API

sh.ui = {}
sh.ui.all = {}

function sh.ui.sorter(a, b)
  if a.focus ~= b.focus then
    return a.focus > b.focus
  end
  return a.z > b.z
end

function sh.ui.draw(...)
  local elements = {...}
  local flushed = false
  for k,v,i in sh.table.sortedPairs(elements, function(a, b) return a.z < b.z end) do
    local ret = v:redraw():flush()
    flushed = flushed or ret
  end
  return flushed
end

function sh.ui.foreground(...)
  local elements = {...}
  for k,v,i in sh.table.sortedPairs(elements, function(a, b) return a.z < b.z end) do
    v:foreground()
  end
end

function sh.ui.handle(event, ...)
  local elements = {...}
  for k,v,i in sh.table.sortedPairs(elements, sh.ui.sorter) do
    if v:handle(event) then
      return true
    end
  end
  return false
end

function sh.ui.intersects(x, y, x1, x2, y1, y2)
  return x >= x1 and x <= x2 and y >= y1 and y <= y2
end

---------- Base Object

sh.ui.base = {}

function sh.ui.base:constructor(parent, x, y, w, h)
  x = x or 1
  y = y or 1
  w = w or parent.w
  h = h or parent.h

  table.insert(sh.ui.all, self)
  self.id = #sh.ui.all

  if type(self.padding) ~= 'table' then
    self.padding = { x = self.padding or 0, y = self.padding or 0 }
  end
  self.padding = sh.table.default(self.padding, { x = 0, y = 0 })
  self.padding.t = self.padding.t or self.padding.y
  self.padding.b = self.padding.b or self.padding.y
  self.padding.l = self.padding.l or self.padding.x
  self.padding.r = self.padding.r or self.padding.x

  self.z = self.z or 0

  self.elements = self.elements or {}
  self.held = self.held or false
  self.active = false

  self.scroll = type(self.scroll) == 'table' and self.scroll or { x = not not self.scroll, y = not not self.scroll }
  if self.scroll.bounded ~= false then
    self.scroll.bounded = true
  end

  self.animators = self.animators or {}

  self:reposition(x, y, w, h, parent)

  self:redraw()
end

sh.ui.base._getX, sh.ui.base._getY, sh.ui.base._getW, sh.ui.base._getWidth, sh.ui.base._getH, sh.ui.base._getHeight = table.unpack(sh.table.rep(function(obj, index) return obj.outer[index] end, 6))

function sh.ui.base:reposition(x, y, w, h, parent)
  if sh.class.of(parent, sh.ui.base) then
    self.parent = parent
    parent = self.parent.inner
  end

  if self.parent then
    nx, ny, nw, nh = self.parent:repositionChild(self, x, y, w, h)
    if nx ~= false then
      x = nx or x
      y = ny or y
      w = nw or w
      h = nh or h
    else
      return
    end
  end

  if self.outer then
    self.outer.reposition(x, y, w, h, parent)
  else
    self.outer = parent:child(x, y, w, h, false)
  end
  local px, py, pw, ph = self.padding.l + 1, self.padding.t + 1, self.outer.w - self.padding.l - self.padding.r, self.outer.h - self.padding.t - self.padding.b
  if self.inner then
    self.inner.reposition(px, py, pw, ph, parent)
  else
    self.inner = self.outer:child(px, py, pw, ph, false)
  end
  self.content = self:calculateContent()
  self:redraw()
end

function sh.ui.base:draw() end

function sh.ui.base:redraw()
  if self.clear then
    self.outer.setBackgroundColor(self.clear)
    self.outer.clear()
    self.inner.setBackgroundColor(self.clear)
    self.inner.clear()
  end

  local current = term.current()
  term.redirect(self.inner)

  self:draw(self.inner, self.outer)
  if self.drawAfter then self:drawAfter(self.inner, self.outer) end
  self.inner:flush()

  term.redirect(current)

  return self
end

function sh.ui.base:flush(up)
  if up or self.immediate then
    return self:flushUp()
  end
  self.outer:flush()
  return self
end

function sh.ui.base:flushUp()
  self.outer:flushUp()
  return self
end

function sh.ui.base:foreground()
  for k,v in ipairs(self.animators) do
    v = sh.table.default(v, { actor = self, tick = 1, created = sh.routine.tick, speed = 1 })
    if not v.paused then
      if v.callback(v) then
        if v.finish == 'pause' then
          v.paused = true
        elseif v.finish ~= 'forward' then
          table.remove(self.animators, k)
        end
      else
        v.tick = v.tick + v.speed
      end
    end
  end
end

function sh.ui.base:handle(event)
  if self.disabled then
    return
  end

  if event[1] == "mouse_click" then
    if self:intersects(event[3], event[4]) then
      self.held = true
      self:redraw():flushUp()
      return self:pressed(event)
    end
  elseif event[1] == "mouse_up" and self.held then
    self.held = false
    self:redraw():flush()
    if self:intersects(event[3], event[4]) then
      return self:clicked(event)
    else
      self:flushUp()
    end
  elseif event[1] == "mouse_drag" and self.held then
    return self:dragged(event)
  elseif event[1] == "mouse_scroll" then
    if self:intersects(event[3], event[4]) then
      return self:scrolled(event)
    end
  end

  return false
end

function sh.ui.base:child(class, obj, ...)
  local el = class(obj, self, ...)
  self:insert(el)
  return el
end

function sh.ui.base:insert(...)
  sh.table.merge(self.elements, {...})

  for k,v in pairs({...}) do
    if v.parent ~= self then
      v:reposition(v.x, v.y, v.w, v.h, self)
    end
  end
  self:redraw()
end

function sh.ui.base:repositionChild(child, x, y, w, h)
  return nil
end

function sh.ui.base:calculateContent(forcew, forceh)
  local content = { x1 = 1, y1 = 1, x2 = 1, y2 = 1 }
  for k,v in pairs(self.elements) do
    content.x1 = math.min(content.x1, v.outer.x1)
    content.x2 = math.max(content.x2, v.outer.x2)
    content.y1 = math.min(content.y1, v.outer.y1)
    content.y2 = math.max(content.y2, v.outer.y2)
  end
  content.w = (content.x2 - content.x1) + 1
  content.h = (content.y2 - content.y1) + 1
  return content
end

function sh.ui.base:intersects(x, y)
  return sh.ui.intersects(x, y, self.inner.sx1, self.inner.sx2, self.inner.sy1, self.inner.sy2)
end

function sh.ui.base:pressed(event)
  return true
end
function sh.ui.base:clicked(event)
  if self.act then self:act(event) end
  return true
end
function sh.ui.base:dragged(event)
  return false
end
function sh.ui.base:scrolled(event)
  return false
end

function sh.ui.base:animate(animation, options)
  animation = sh.table.replace(animation, options)
  if animation.group then
    local groups = sh.table.extract(self.animators, 'group')
    if sh.table.contains(groups, animation.group) then
      return false
    end
  end

  table.insert(self.animators, animation)
  animation.id = #self.animators
  return animation
end

function sh.ui.base:theme(...)
  return sh.table.get({
    bg = (self.held ~= self.active) and colors.white or colors.lightGray,
    fg = self.disabled and colors.gray or colors.black
  }, ...)
end

sh.ui.base = sh.class.new(sh.ui.base, 'UI Base Object')

---------- Container Object

sh.ui.container = {}

function sh.ui.container:constructor(parent, x, y, w, h)
  self.translate = self.translate or {}
  self.translate.x = self.translate.x or 0
  self.translate.y = self.translate.y or 0
end

function sh.ui.container:draw()
  sh.ui.draw(table.unpack(self.elements))
end

function sh.ui.container:foreground()
  sh.ui.foreground(table.unpack(self.elements))
  sh.ui.base.foreground(self)
end

function sh.ui.container:handle(event)
  if not self.greedy then
    if event[1] == "mouse_click" and not self:intersects(event[3], event[4]) then
      return false
    end
  end

  local ret = sh.ui.handle(event, table.unpack(self.elements))
  if not ret then
    sh.ui.base.handle(self, event)
  end
  self:flush()

  return ret
end

function sh.ui.container:insert(...)
  local ret = sh.ui.base.insert(self, ...)
  self:arrange()
  return ret
end

function sh.ui.container:arrange(grid)
  self.arranging = true
  if grid ~= nil then
    if grid == 'hstack' then
      grid = {{}}
      for k,v in pairs(self.elements) do
        table.insert(grid[1], { element = v, width = 'hug', height = 'fill' })
      end
    elseif grid == 'hfstack' then
      grid = {{}}
      for k,v in pairs(self.elements) do
        table.insert(grid[1], { element = v, width = 'fixed', height = 'fill' })
      end
    elseif grid == 'h' then
      grid = {{}}
      for k,v in pairs(self.elements) do
        table.insert(grid[1], { element = v, width = 'fill', height = 'fill' })
      end
    elseif grid == 'vstack' then
      grid = {}
      for k,v in pairs(self.elements) do
        table.insert(grid, {{ element = v, height = 'hug', width = 'fill' }})
      end
    elseif grid == 'vfstack' then
      grid = {}
      for k,v in pairs(self.elements) do
        table.insert(grid, {{ element = v, height = 'fixed', width = 'fill' }})
      end
    elseif grid == 'v' then
      grid = {}
      for k,v in pairs(self.elements) do
        table.insert(grid, {{ element = v, height = 'fill', width = 'fill' }})
      end
    end
    self.grid = grid
  end
  if not self.grid then
    return
  end

  local sx = (type(self.grid.spacing) == 'table' and self.grid.spacing.x or self.grid.spacing) or 0
  local sy = (type(self.grid.spacing) == 'table' and self.grid.spacing.y or self.grid.spacing) or 0

  local fills = {}
  local available = self.inner.h
  local fillh = 0
  for k,row in ipairs(self.grid) do
    row.fills = {}
    row.available = self.inner.w
    row.fillw = 0
    row.minh = 0
    for k2,cell in ipairs(row) do
      cell.element = cell.element or sh.ui.all[cell.id]
      if cell.element.parent == self then
        cell.width = cell.width or cell.w or 'fixed'
        if cell.width == 'fill' then
          cell.fillw = true
          table.insert(row.fills, cell)
        else
          cell.targetW = cell.element.width
          row.available = row.available - cell.targetW - sx
        end

        cell.height = cell.height or cell.h or 'fixed'
        if cell.height == 'fill' then
          row.fillh = true
        else
          cell.targetH = cell.element.height
          row.minh = math.max(row.minh, cell.targetH)
        end
      end
    end
    if #row.fills > 0 then
      row.fillw = (row.available / #row.fills) - (sx * #row.fills)
    end
    if not row.fillh then
      available = available - row.minh - sy
    else
      table.insert(fills, row)
    end
  end
  if #fills > 0 then
    fillh = (available / #fills) - (sy * #fills)
  end

  for k,row in ipairs(self.grid) do
    local h = row.fillh and fillh or row.minh
    for k2,cell in ipairs(row) do
      cell.targetW = cell.fillw and row.fillw or cell.targetW
      cell.targetH = cell.height ~= 'fill' and cell.targetH or h

      local content = cell.element:calculateContent(cell.targetW, cell.targetH)
      if cell.width == 'hug' then
        cell.targetW = content.w + cell.element.padding.l + cell.element.padding.r
      end
      if cell.height == 'hug' then
        cell.targetH = content.h + cell.element.padding.t + cell.element.padding.b
      end
    end
  end

  local y = 1
  for k,row in ipairs(self.grid) do
    local x = 1
    local h = 0
    for k2,cell in ipairs(row) do
      cell.element:reposition(x, y, cell.targetW, cell.targetH)
      x = x + cell.targetW + sx
      h = math.max(h, cell.targetH)
    end
    y = y + h + sy
  end
  self:redraw():flush()
  self.arranging = nil
end

function sh.ui.container:reposition(child, x, y, w, h)
  local ret = sh.ui.base.reposition(self, child, x, y, w, h)
  self:arrange()
  return ret
end

function sh.ui.container:repositionChild(child, x, y, w, h)
  if not self.grid then
    return
  end
  for k,row in ipairs(self.grid) do
    for k2,cell in ipairs(row) do
      if cell.element == child or cell.id == child.id then
        if not self.arranging then
          self.arranging = true
          cell.element:reposition(self.outer.x, self.outer.y, w, h)
          self:arrange()
          self.arranging = nil
          return false
        end
      end
    end
  end
end

function sh.ui.container:offset(x, y)
  for k,v in pairs(self.elements) do
    v:reposition(v.x + x - self.translate.x, v.y + y - self.translate.y, v.w, v.h)
  end
  self.translate.x = x
  self.translate.y = y
  self:redraw()
end

function sh.ui.container:dragged(event)
  if self.scroll.x or self.scroll.y then
    local x = self.translate.x
    local y = self.translate.y
    if self.scroll.x then
      x = x + sh.mouse.relative.x
    end
    if self.scroll.y then
      y = y + sh.mouse.relative.y
    end
    if self.scroll.bounded then
      x = sh.math.clamp(x, -self.content.x2, -self.content.x1)
      y = sh.math.clamp(y, -self.content.y2 + self.inner.h, -self.content.y1)
    end
    self:offset(x, y)
    return true
  end
end

function sh.ui.container:scrolled(event)
  if self.scrollHorizontal then
    if self.scroll.x then
      local x = self.translate.x
      if self.scroll.x then
        x = x - event[2]
      end
      if self.scroll.bounded then
        x = sh.math.clamp(x, -self.content.x2, -self.content.x1)
      end
      self:offset(x, self.translate.y)
      return true
    end
  else
    if self.scroll.y then
      local y = self.translate.y
      if self.scroll.y then
        y = y - event[2]
      end
      if self.scroll.bounded then
        y = sh.math.clamp(y, -self.content.y2 + self.inner.h, -self.content.y1)
      end
      self:offset(self.translate.x, y)
      return true
    end
  end
end

sh.ui.container = sh.class.new(sh.ui.container, 'Container', sh.ui.base)

---------- Card Object

sh.ui.card = {}

function sh.ui.card:constructor(parent, x, y, w, h)
  if self.title and not sh.class.of(self.title, sh.ui.base) then
    local init = self.title
    if type(init) ~= 'table' then
      init = { text = self.title, padding = 1 }
    end
    self.title = self:child(sh.ui.text, init, 1, 1, w, h)
  end
end

function sh.ui.card:draw(inner, outer)
  local bg, fg = self:theme('bg', 'fg')
  inner.setBackgroundColor(bg)
  inner.setTextColor(fg)
  inner.clear()

  if sh.class.of(self.title, sh.ui.base) then
    self.title.clear = bg
    self.title.color = fg
    sh.ui.draw(self.title)
  end
end

sh.ui.card = sh.class.new(sh.ui.card, 'Card', sh.ui.base)

---------- Text Object

sh.ui.text = {}

function sh.ui.text:draw()
  term.setBackgroundColor(self.clear or colors.black)
  term.setTextColor(self.color or colors.white)
  term.clear()

  local writer = term.write
  if self.align == 'right' then
    writer = sh.graphics.rightWrite
    term.setCursorPos(self.inner.w, 1)
  elseif self.align == 'center' then
    writer = sh.graphics.centerWrite
    term.setCursorPos(math.ceil(self.inner.w/2), 1)
  else
    term.setCursorPos(1, 1)
  end
  sh.graphics.write(self.text, nil, writer, false)
end

function sh.ui.text:calculateContent(forcew, forceh)
  local iw = forcew or self.inner.w

  local text = self.text
  if type(text) == 'table' then
    text = sh.table.implode(text, '\t')
  else
    text = tostring(text)
  end
  local lines = sh.string.wrap(text, math.max(iw, 1))

  local longest = ''
  for k,v in pairs(lines) do
    if string.len(longest) < string.len(v) then
      longest = v
    end
  end
  local w = string.len(longest)

  local content = {}
  if self.align == 'center' then
    local x = sh.graphics.alignCenter(longest, math.ceil(iw/2))
    content = { x1 = x, x2 = x + w }
  elseif self.align == 'right' then
    local x = sh.graphics.alignRight(longest, iw)
    content = { x1 = x, x2 = iw }
  else
    content = { x1 = 1, x2 = w }
  end

  content.y1 = 1
  content.y2 = #lines
  content.w = (content.x2 - content.x1) + 1
  content.h = (content.y2 - content.y1) + 1

  return content
end

sh.ui.text = sh.class.new(sh.ui.text, 'Text', sh.ui.base)

---------- Animators

sh.ui.animations = {}

function sh.ui.animations.speed(constant)
  return function (tick, dist, animation) return constant end
end

function sh.ui.animations.variable(start, change)
  if type(change) == 'number' then
    local constant = change
    change = function (prev, start, tick, dist) return prev + constant end
  end
  return function (tick, dist, animation)
    if tick == 1 then
      return start
    end
    animation.lastSpeed = change(animation.lastSpeed or start, start, tick, dist)
    return animation.lastSpeed
  end
end

function sh.ui.animations.time(ticks)
  return function (tick, dist, animation) return dist / ticks end
end

function sh.ui.animations.reposition(ui, x, y, w, h, speed)
  x = x or ui.x
  y = y or ui.y
  w = w or ui.w
  h = h or ui.h

  local dx = x - ui.x
  local dy = y - ui.y
  local dw = w - ui.w
  local dh = h - ui.h
  local dist = math.max(math.abs(dx), math.abs(dy), math.abs(dw), math.abs(dh), 1)
  
  if type(speed) == 'number' then
    speed = sh.ui.animations.speed(speed)
  end

  local total = 0
  local length = 0
  local temp = {}
  while total < dist and length < 1000 * sh.routine.ups do
    total = total + speed(length + 1, dist, temp)
    length = length + 1
  end
  
  return {
    length = length,
    callback = function (animation)
      local s = speed(animation.tick, dist, animation)
      if animation.tick < 1 or animation.tick > animation.length then
        return true
      elseif animation.tick == animation.length then
        animation.actor:reposition(x, y, w, h)
        animation.actor:redraw():flush()
        return true
      end

      local dx = dx / dist * s * animation.speed
      local dy = dy / dist * s * animation.speed
      local dw = dw / dist * s * animation.speed
      local dh = dh / dist * s * animation.speed

      animation.dx = (animation.dx or 0) + dx
      dx = math.floor(animation.dx)
      if dx ~= 0 then animation.dx = animation.dx - math.floor(animation.dx) end
      animation.dy = (animation.dy or 0) + dy
      dy = math.floor(animation.dy)
      if dy ~= 0 then animation.dy = animation.dy - math.floor(animation.dy) end
      animation.dw = (animation.dw or 0) + dw
      dw = math.floor(animation.dw)
      if dw ~= 0 then animation.dw = animation.dw - math.floor(animation.dw) end
      animation.dh = (animation.dh or 0) + dh
      dh = math.floor(animation.dh)
      if dh ~= 0 then animation.dh = animation.dh - math.floor(animation.dh) end

      animation.actor:reposition(animation.actor.x + dx, animation.actor.y + dy, animation.actor.w + dw, animation.actor.h + dh)
      animation.actor:redraw():flush()
    end
  }
end