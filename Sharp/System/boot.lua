--Everything internal for Sharp can be found inside this one table.
_G.sh = { boot = { time = os.clock() } }

--Rudimentary logging and error handling until additional modules are loaded.
local bootlog = {}
local module = shell.getRunningProgram()
function sh.boot.log(message, color, severity, origin)
  local entry = { text = message, color = color, severity = severity or 0, origin = origin or module or shell.getRunningProgram(), time = os.clock() - sh.boot.time, index = #bootlog + 1 }
  table.insert(bootlog, entry)
  if sh.log and sh.log.boot then
    sh.log.boot.add(entry)
  end
end

function sh.boot.error(...)
  local params = {...}
  sh.boot.log('Sharp is unable to continue booting.', colors.red, 2)
  sh.boot.log(params[1], colors.red, 2)
  if sh.err then
    sh.err.handler(function() error(params[1]) end, sh.err.fatal)
  else
    term.setTextColor(colors.red)
    print("Sharp is unable to continue booting.")
    if not sh.log then
      term.setTextColor(colors.gray)
      print("The error occurred before the logging module could be loaded, below is the current bootlog:")
      for k,v in pairs(bootlog) do
        print(v.origin .. ' - ' .. v.text)
      end
    end
    print("")
    error(table.unpack(params))
  end
end

sh.boot.log("Basic logger initialised.", colors.green)
sh.boot.log("Booting Sharp...", colors.lime)

--Checking for colour terminal.
if not term.isColor() then
  sh.boot.error("Sharp requires a color terminal.")
end

local args = {...}
--Keep and remove metadata passed from a startup wrapper, if present.
if _G.shWrapper then
  sh.wrapper = _G.shWrapper
  _G.shWrapper = nil
  if sh.wrapper.args then
    args = sh.wrapper.args
  end
end

--This function loads a system module.
function sh.boot.module(file, env)
  sh.boot.log("Loading system module " .. file, colors.blue)

  local path = fs.combine(shell.getRunningProgram(), '../' .. file)
  local code, reason = fs.open(path, "r")
  if not code then
    sh.boot.error(reason, 2)
  end

  module = path
  local environment = setmetatable(env or {}, { __index = _ENV })
  local fun, err = load(code.readAll(), '@' .. fs.getName(path), nil, environment)
  code.close()

  if not fun then
    sh.boot.error(err, 2)
  end
  
  local result = { xpcall(fun, function(err) return debug.traceback(err, 2) end) }
  if not result[1] then
    sh.boot.error(result[2])
  end

  module = shell.getRunningProgram()
  return fun, table.unpack(result, 2)
end

--Starting Sharp
term.setBackgroundColor(colors.black)
term.setTextColor(colors.white)
term.setCursorPos(1,1)
term.clear()
print("Starting Sharp")

--Start loading basic modules.
sh.boot.module("args.lua", { args = args })
sh.boot.module("paths.lua", { root = fs.combine(shell.getRunningProgram(), '../../..'), boot = shell.getRunningProgram() })
sh.boot.module("about.lua")
sh.boot.module("log.lua", { boot = { current = bootlog, file = sh.args.boot.switches.f, print = sh.args.boot.switches.p } })

--Start loading everything else.
sh.boot.module("table.lua")
sh.boot.module("string.lua")
sh.boot.module("math.lua")
sh.boot.module("class.lua")
sh.boot.module("config.lua")
sh.boot.module("graphics.lua")
sh.boot.module("graphics/pixel.lua")
sh.boot.module("graphics/canvas.lua")
sh.boot.module("mouse.lua")
sh.boot.module("error.lua")
sh.boot.module("ui.lua")
sh.boot.module("users.lua")
sh.boot.module("apps.lua")
sh.boot.module("instances.lua")
sh.boot.module("api.lua")
sh.boot.module("env.lua")
sh.boot.module("routine.lua")
sh.boot.module("launch.lua")

local time = sh.about.clock()
if time == 0 then
  time = "<0.05"
end
sh.boot.log("Loading complete in " .. time .. " seconds.", colors.lime)
if sh.log.boot.print then
  term.setCursorBlink(false)
  sh.boot.log("Press any key to start Sharp.", colors.white)
  while true do
    local event = {os.pullEvent("key")}
    if not event[3] then
      break
    end
  end
  print("")
end

--Run Sharp with a better error handler.
sh.err.handler(sh.routine.start, sh.err.fatal)