sh.api = {}

sh.api.about = sh.about

sh.api.table = sh.table
sh.api.string = sh.string
sh.api.math = sh.math

sh.api.log = { create = sh.log.create, runtime = sh.log.runtime, file = sh.log.file, print = sh.log.print }

sh.api.routine = {}
sh.api.routine._get = function (obj, key)
  if key == 'ups' or key == 'updateRate' or key == 'tick' or key == 'tickMod' then
    return true, sh.routine[key]
  end
end
sh.api.routine = sh.class.singleton(sh.api.routine)

sh.api.users = {}
sh.api.users._get = function (obj, key)
  if key == 'all' or key == 'current' or key == 'active' then
    return true, sh.users[key]
  end
end
sh.api.users = sh.class.singleton(sh.api.users)

sh.api.apps = {}
sh.api.users._get = function (obj, key)
  if key == 'all' then
    return true, sh.users[key]
  end
end
sh.api.apps = sh.class.singleton(sh.api.apps)

sh.api.graphics = sh.table.unset(sh.table.clone(sh.graphics), 'main', 'window', 'init')
sh.api.graphics.term = { getSize = term.native().getSize }
sh.api.graphics._get = function (obj, key)
  if key == "width" or key == "height" then
    local w, h = term.getSize()
    return true, key == "width" and w or h
  end
end
sh.api.graphics = sh.class.singleton(sh.api.graphics)

sh.api.ui = sh.table.unset(sh.table.clone(sh.ui), 'all')