---------- App Internal API

sh.apps = {}

sh.apps.all = {}
sh.apps.shell = sh.config['apps.shell']
sh.apps.login = sh.config['apps.login']

function sh.apps.loadAll()
  local all = {}
  for k,v in pairs(fs.list(sh.path.apps)) do
    all[v] = sh.app({ path = fs.combine(sh.path.apps, v) })
  end
  return all
end

sh.boot.log("Created load all apps function.", colors.green)

---------- App Object

sh.app = {}

function sh.app:constructor()
  self.longname = fs.getName(self.path)
  self.firstRun = not fs.exists(fs.combine(self.path, "runcheck.dat"))

  self.environment = sh.table.clone(sh.env)
  
  local code, reason = fs.open(fs.combine(self.path, "main.lua"), "r")
  if not code then
    sh.err.appLoad(reason, { app = self })
    return nil
  end

  local ok, err = load(code.readAll(), '=' .. self.longname, nil, self.environment)
  code.close()
  if ok then
    ok()
  else
    sh.boot.log("An error has occured loading " .. self.longname, colors.red, 2)
    sh.boot.log("Printed below is a stack trace:", colors.red, 2)
    sh.boot.log(err, colors.red, 2)
    sh.err.appLoad(err, { app = self })
    return nil
  end
  
  self.routine = self.environment.app
  self.settings = self.routine.settings()
  
  self.name = self.settings.name or self.longname
  self.priority = self.settings.priority or 0
  
  return self
end

sh.app = sh.class.new(sh.app, 'App')

sh.boot.log("Created app class.", colors.green)