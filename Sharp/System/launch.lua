sh.graphics.init(1, 1, sh.graphics.width, sh.graphics.height, false)

sh.apps.all = sh.apps.loadAll()
sh.users.all = sh.users.loadAll()
sh.instances.system = sh.instances.layer()

for k,v in pairs(sh.apps.all) do
  sh.err.handler(v.routine.startup, sh.err.appRun, { app = v }, 'startup')
  sh.boot.log("Running startup function for " .. v.name, colors.cyan)
end

--Create the shell instance to start with.
sh.err.handler(function() sh.instances.shell = sh.instance({
  canvas = sh.graphics.canvas({}, sh.graphics.main, 1, 1, sh.graphics.width, sh.graphics.height, false)
}, nil, sh.apps.shell, 3) end, sh.err.fatal)

--Create the login instance underneath the shell.
--TODO the shell should probs handle this
--sh.err.handler(function() sh.instances.login = sh.instance({}, nil, sh.apps.login, 3) end, sh.err.fatal)