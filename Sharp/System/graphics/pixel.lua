---------- Pixel Graphics API

sh.graphics.pixel = {}
sh.graphics.pixel.positions = {}
sh.graphics.pixel.inverses = {}

--Credit to http://www.computercraft.info/forums2/index.php?/topic/25340-cc-176-easy-drawing-characters/
function sh.graphics.pixel.char(tl, tr, l, r, bl, br)
  local data = 128
  data = data + ((tl ~= br) and 1 or 0)
  data = data + ((tr ~= br) and 2 or 0)
  data = data + ((l ~= br) and 4 or 0)
  data = data + ((r ~= br) and 8 or 0)
  data = data + ((bl ~= br) and 16 or 0)
  return string.char(data), br
end

function sh.graphics.pixel.from(char)
  if not char then
    return
  elseif type(char) == 'string' then
    char = string.byte(char)
    if not char then
      return
    end
  end
  if sh.table.contains({0,9,10,13,32}, char) then
    char = 128
  end
  if char < 128 or char > 159 then
    return
  end
  char = char - 128

  local ret = {}
  for i=0,4 do
    table.insert(ret, bit.band(char, math.pow(2, i)) > 0)
  end
  return unpack(ret)
end

function sh.graphics.pixel.setPixelPos(a, b)
  local hash = sh.table.hash(term.current())
  a = ((a - 1) % 2) + 1
  b = ((b - 1) % 3) + 1
  sh.graphics.pixel.positions[hash] = { x = a, y = b }
  return a, b
end

function sh.graphics.pixel.getPixelPos()
  local hash = sh.table.hash(term.current())
  local pos = sh.graphics.pixel.positions[hash] or {}
  return pos.x or 1, pos.y or 1
end

function sh.graphics.pixel.setCursorPos(x, y)
  term.setCursorPos(math.ceil(x / 2), math.ceil(y / 3))
  sh.graphics.pixel.setPixelPos(x, y)
end

function sh.graphics.pixel.getCursorPos()
  local x, y = term.getCursorPos()
  local a, b = sh.graphics.pixel.getPixelPos()
  return (x * 2) + a, (y * 2) + b
end

function sh.graphics.pixel.setInverted(invert, x, y)
  local hash = sh.table.hash(term.current())
  local a, b = term.getCursorPos()
  x = x or a
  y = y or b

  sh.graphics.pixel.inverses[hash] = {}
  sh.graphics.pixel.inverses[hash][x] = {}
  sh.graphics.pixel.inverses[hash][x][y] = invert
end

function sh.graphics.pixel.getInverted(x, y)
  local hash = sh.table.hash(term.current())
  local a, b = term.getCursorPos()
  x = x or a
  y = y or b

  local inverses = sh.graphics.pixel.inverses[hash] or {}
  local inverses1 = inverses[x] or {}
  return inverses1[x] or false
end

function sh.graphics.pixel.get(x, y)
  local char = sh.graphics.get(x, y)
  local ret = { sh.graphics.pixel.from(char) }
  if ret[1] == nil then
    return
  end

  local invert = sh.graphics.pixel.getInverted(x, y)
  ret = sh.table.map(ret, function(bool) return bool ~= invert end)
  table.insert(ret, invert)
  return unpack(ret)
end

function sh.graphics.pixel.set(tl, tr, l, r, bl, br)
  local char, invert = sh.graphics.pixel.char(tl, tr, l, r, bl, br)
  local x, y = term.getCursorPos()
  local fg, bg = term.getTextColor(), term.getBackgroundColor()
  if invert then
    term.setTextColor(bg)
    term.setBackgroundColor(fg)
  end
  term.write(char)
  term.setCursorPos(x, y)
  sh.graphics.pixel.setInverted(invert, x, y)
  term.setTextColor(fg)
  term.setBackgroundColor(bg)
end

function sh.graphics.pixel.toggle(force, color)
  local w, h = term.getSize()
  local x, y = term.getCursorPos()
  if x > w or y > h then
    return
  end

  local fg, bg = term.getTextColor(), term.getBackgroundColor()
  if color == true then
    color = fg
  end
  local cfg, cbg = select(2, sh.graphics.get())
  color = color or cfg

  local a, b = sh.graphics.pixel.getPixelPos()
  local pixels = { sh.graphics.pixel.get() }
  if pixels[1] == nil then
    pixels = sh.table.fill(false, 6)
  end
  local index = 1 + ((a - 1) % 2) + ((b - 1) * 2)
  local before = pixels[index]
  if force == nil then
    pixels[index] = not pixels[index]
  else
    pixels[index] = force
  end
  if pixels[index] == before then
    return
  end
  term.setTextColor(pixels[index] and color or cfg)
  term.setBackgroundColor(cbg)
  sh.graphics.pixel.set(unpack(pixels))
  if pixels[index] then
    term.setTextColor(fg)
  end
  term.setBackgroundColor(bg)
end

function sh.graphics.pixel.add(color)
  return sh.graphics.pixel.toggle(true, color)
end

function sh.graphics.pixel.sub()
  return sh.graphics.pixel.toggle(false)
end
