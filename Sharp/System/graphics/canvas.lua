---------- Canvas API (with lazy blitting)

local color_toBlit = {}
local color_fromBlit = {}
local color_toPacked = {}
local color_fromPacked = {}
for i=0,15 do
  color_toBlit[2^i] = string.format("%x", i)
  color_fromBlit[string.format("%x", i)] = 2^i

  color_toPacked[2^i] = i
  color_fromPacked[i] = 2^i
end
local space = string.byte(' ')

sh.graphics.canvas = {}

----------

function sh.graphics.canvas.pack(char, fg, bg, mask)
  if type(char) == "string" then
    char = string.byte(char)
  end
  mask = mask and 1 or 0
  return bit.bor(char, bit.blshift(color_toPacked[fg], 8), bit.blshift(color_toPacked[bg], 12), bit.blshift(mask, 16))
end

function sh.graphics.canvas.packModify(packed, char, fg, bg, mask)
  if char ~= nil then
    if type(char) == "string" then
      char = string.byte(char)
    end
    packed = bit.bor(bit.band(packed, bit.bnot(0xFF)), char)
  end
  if fg ~= nil then
    packed = bit.bor(bit.band(packed, bit.bnot(bit.blshift(0xF, 8))), bit.blshift(fg, 8))
  end
  if bg ~= nil then
    packed = bit.bor(bit.band(packed, bit.bnot(bit.blshift(0xF, 12))), bit.blshift(bg, 12))
  end
  if mask ~= nil then
    mask = mask and 1 or 0
    packed = bit.bor(bit.band(packed, bit.bnot(bit.blshift(0x1, 16))), bit.blshift(mask, 16))
  end
  return packed
end

function sh.graphics.canvas.unpack(packed)
  local mask = bit.band(bit.brshift(packed, 16), 0x1) > 0 and true or false
  local bg = color_fromPacked[bit.band(bit.brshift(packed, 12), 0xF)]
  local fg = color_fromPacked[bit.band(bit.brshift(packed, 8), 0xF)]
  local char = string.char(bit.band(packed, 0xFF))
  return char, fg, bg, mask
end

function sh.graphics.canvas.writerTerm(canvas, packed, target, si, sx, sy, tx, ty, chain, pass)
  if packed then
    pass = pass or { count = 0, x = tx, y = ty, char = {}, bg = {}, fg = {} }
    pass.count = pass.count + 1

    local index = pass.count
    local char, fg, bg = sh.graphics.canvas.unpack(packed)
    pass.char[index] = char
    pass.fg[index] = color_toBlit[fg]
    pass.bg[index] = color_toBlit[bg]

    canvas:clean(packed, si)

    if chain then
      return pass
    end
  end
  if pass then
    target.setCursorPos(pass.x, pass.y)
    target.blit(table.concat(pass.char), table.concat(pass.fg), table.concat(pass.bg))
  end
end

function sh.graphics.canvas.writerCanvas(canvas, packed, target, si, sx, sy, tx, ty, chain, pass)
  if packed then
    target:set(packed, tx, ty)
    canvas:clean(packed, si)
  end
end

----------

function sh.graphics.canvas._get(obj, key)
  if key == "x1" then
    return true, (obj.x)
  elseif key == "y1" then
    return true, (obj.y)
  elseif key == "width" then
    return true, (obj.w)
  elseif key == "height" then
    return true, (obj.h)
  elseif key == "x2" then
    return true, (obj:getEndPosition())
  elseif key == "y2" then
    return true, (select(2, obj:getEndPosition()))
  elseif key == "sx1" then
    return true, (obj:getScreenPosition())
  elseif key == "sx2" then
    return true, (obj:getScreenEndPosition())
  elseif key == "sy1" then
    return true, (select(2, obj:getScreenPosition()))
  elseif key == "sy2" then
    return true, (select(2, obj:getScreenEndPosition()))
  end
end

function sh.graphics.canvas:constructor(parent, x, y, w, h)
  self.children = {}
  self.data = {}
  self.last = {}
  self.markings = { forward = {}, reverse = {} }
  self.cx = 1
  self.cy = 1
  self.cfg = term.getTextColor()
  self.cbg = term.getBackgroundColor()

  local canvas = self
  self.term = setmetatable({}, { __index = function (table, key)
    return function(...) return canvas[key](canvas, ...) end
  end })

  self:reposition(x, y, w, h, parent)
end

function sh.graphics.canvas:reposition(x, y, w, h, parent)
  local nw, nh = term.native().getSize()
  local ox, oy, ow, oh = self.x, self.y, self.w or 0, self.h or 0
  self.x = x or ox or 1
  self.y = y or oy or 1
  local isParentCanvas = sh.class.of(parent, sh.graphics.canvas)
  self.w = w or ow or (isParentCanvas and parent.w or nw)
  self.h = h or oh or (isParentCanvas and parent.h or nh)

  local clear = self.cbg
  if self.h > oh then
    self:drawFilledBox(1, oh+1, self.w, self.h, clear)
  elseif self.h < oh then
    self:drawFilledBox(1, self.h+1, self.w, oh, false)
  end

  if self.w > ow then
    self:drawFilledBox(ow+1, 1, self.w, self.h, clear)
  elseif self.w < ow then
    self:drawFilledBox(self.w+1, 1, ow, self.h, false)
  end

  if parent then
    self:setParent(parent)
  end
end

function sh.graphics.canvas:setParent(parent)
  if parent == self.parent then
    return
  end
  if sh.class.of(self.parent, sh.graphics.canvas) then
    local children = self.parent.children
    local found, key = sh.table.contains(children, self)
    if found then
      table.remove(children, key)
    end
  end
  self.parent = parent
  if sh.class.of(parent, sh.graphics.canvas) then
    table.insert(parent.children, self)
  end
end

function sh.graphics.canvas:insert(child)
  child:setParent(self)
end

function sh.graphics.canvas:newChild(class, obj, ...)
  obj = obj or {}
  class = class or sh.graphics.canvas
  local el = class(obj, self, ...)
  self:insert(el)
  return el
end

function sh.graphics.canvas:getCursorPos()
  return self.cx, self.cy
end

function sh.graphics.canvas:setCursorPos(cx, cy)
  self.cx = cx
  self.cy = cy
end

function sh.graphics.canvas:getBackgroundColor()
  return self.cbg
end

function sh.graphics.canvas:setBackgroundColor(color)
  self.cbg = color
end

function sh.graphics.canvas:getTextColor()
  return self.cfg
end

function sh.graphics.canvas:setTextColor(color)
  self.cfg = color
end

function sh.graphics.canvas:output(target, x, y, options)
  x = x or self.x
  y = y or self.y
  target = target or self.parent or term.current()
  options = options or {}
  local force = options.force
  if (not force) and (#self.markings.forward <= 0) then
    return
  end
  local writer = options.writer
  if not writer then
    if sh.class.of(target, sh.graphics.canvas) then
      writer = sh.graphics.canvas.writerCanvas
    else
      writer = sh.graphics.canvas.writerTerm
    end
  end
  local _opti_canvas_get = self.get
  local base_x = x-1
  local base_y = y-1
  if options.cut then
    local cut = options.cut
    if cut.relative then
      if cut.x1 then cut.x1 = cut.x1 - base_x end
      if cut.x2 then cut.x2 = cut.x2 - base_x end
      if cut.y1 then cut.y1 = cut.y1 - base_y end
      if cut.y2 then cut.y2 = cut.y2 - base_y end
    end
  end
  local next, pass = nil, nil
  if not force then
    local iterator, result = sh.table.pairsSorted(self.markings.forward)
    for k,v,i in iterator do
      local px, py = self:fromIndex(v)
      local packed = next or _opti_canvas_get(self, v)
      local chain = false
      if packed then
        local _, _, _, mask = sh.graphics.canvas.unpack(packed)
        if mask then
          packed = false
          next = nil
        else
          local v2 = self.markings.forward[result[i+1]]
          if v2 == v+1 then
            next = _opti_canvas_get(self, v2)
            if next and (math.ceil(v / self.w) == math.ceil(v2 / self.w)) then
              local _, _, _, mask2 = sh.graphics.canvas.unpack(next)
              if mask2 then
                next = false
              else
                chain = true
              end
            end
          else
            next = nil
          end
        end
      end
      pass = writer(self, packed, target, v, px, py, base_x + px, base_y + py, chain, pass)
    end
  else
    local w, h = self.w, self.h
    for py=1,h,1 do
      for px=1,w,1 do
        local i = self:toIndex(px, py)
        local packed = next or _opti_canvas_get(self, i)
        if packed then
          local _, _, _, mask = sh.graphics.canvas.unpack(packed)
          if mask then
            packed = false
            next = nil
          elseif i < w then
            next = _opti_canvas_get(self, i + 1)
            if next then
              local _, _, _, mask2 = sh.graphics.canvas.unpack(next)
              if mask2 then
                next = false
              else
                chain = true
              end
            end
          else
            next = nil
          end
        end
        pass = writer(self, packed, target, i, px, py, base_x + px, base_y + py, chain, pass)
      end
    end
  end
  if pass then
    writer(self, false, target, nil, nil, nil, nil, nil, nil, pass)
  end
end

function sh.graphics.canvas:get(x, y)
  if y then
    x = self:toIndex(x, y)
    if not x then return nil end
  end
  return self.data[x]
end

function sh.graphics.canvas:set(packed, x, y, silent)
  if y then
    x = self:toIndex(x, y)
    if not x then return nil end
  end
  self.data[x] = packed
  if packed and (not silent) then
    self:mark(packed, x)
  end
  return packed
end

function sh.graphics.canvas:mod(char, fg, bg, mask, x, y, silent)
  if y then
    x = self:toIndex(x, y)
    if not x then return nil end
  end
  local packed = nil
  if char ~= nil and fg ~= nil and bg ~= nil and mask ~= nil then
    packed = sh.graphics.canvas.pack(char, fg, bg, mask)
  else
    local before = self:get(x)
    packed = sh.graphics.canvas.packModify(before, char, fg, bg, mask)
    if packed == before then
      return nil
    end
  end
  return self:set(packed, x, nil, silent)
end

function sh.graphics.canvas:mark(compare, x, y)
  if y then
    x = self:toIndex(x, y)
    if not x then return nil end
  end
  local typecmp = type(compare)
  if typecmp ~= "boolean" then
    if typecmp == "number" then
      compare = bit.band(compare, 65535)
    end
    local last = self.last[x]
    if type(last) == "number" then
      last = bit.band(last, 65535)
    end
    compare = compare ~= last
  end

  local k = self.markings.reverse[x]
  if not k then
    k = #self.markings.forward + 1
  end
  if compare then
    self.markings.forward[k] = x
    self.markings.reverse[x] = k
  elseif k then
    self.markings.reverse[x] = nil
    self.markings.forward[k] = nil
  end
  return compare
end

function sh.graphics.canvas:clean(packed, x, y)
  if y then
    x = self:toIndex(x, y)
    if not x then return nil end
  end
  self:mark(false, x)
  self.last[x] = packed
end

function sh.graphics.canvas:toIndex(x, y)
  local w, h = self.w, self.h
  if x < 1 or x > w or y < 1 or y > h then
    return nil
  end
  return ((y - 1) * self.w) + x
end

function sh.graphics.canvas:fromIndex(i)
  if i < 1 or i > self.w * self.h then
    return nil
  end
  local y = math.floor((i - 1) / self.w) + 1
  local x = i - (y - 1) * self.w
  return x, y
end

function sh.graphics.canvas:write(text)
  local text = tostring(text)
  local len = string.len(text)
  local _opti_string_rep = string.rep
  local fg = color_toBlit[self.cfg]
  local bg = color_toBlit[self.cbg]
  return self:blit(text, _opti_string_rep(fg, len), _opti_string_rep(bg, len))
end

function sh.graphics.canvas:blit(char, fg, bg, mask)
  local i = 1
  local _opti_string_sub = string.sub
  local _opti_canvas_mod = self.mod
  while true do
    local cchar, cfg, cbg, cmask = nil, nil, nil, nil
    if char then
      local charHere = _opti_string_sub(char, i, i)
      if charHere ~= '' then
        cchar = charHere
      end
    end
    if fg then
      local fgHere = _opti_string_sub(fg, i, i)
      if fgHere ~= '' then
        cfg = color_fromBlit[fgHere]
      end
    end
    if bg then
      local bgHere = _opti_string_sub(bg, i, i)
      if bgHere ~= '' then
        cbg = color_fromBlit[bgHere]
      end
    end
    if mask then
      local maskHere = _opti_string_sub(mask, i, i)
      if maskHere ~= '' then
        cmask = maskHere ~= '0'
      end
    end
    if cchar == nil and cfg == nil and cbg == nil and cmask == nil then
      return
    end
    _opti_canvas_mod(self, cchar, cfg, cbg, cmask, self.cx, self.cy)
    self.cx = self.cx + 1
    i = i + 1
  end
end

function sh.graphics.canvas:clear()
  return self:drawFilledBox(1, 1, self.w, self.h, self.cbg)
end

function sh.graphics.canvas:clearLine()
  local cx, cy = self.cx, self.cy
  local ret = self:drawFilledBox(1, cy, self.w, cy, self.cbg)
  self:setCursorPos(cx, cy)
  return ret
end

function sh.graphics.canvas:scroll(y, x)
  x = x or 0
  y = y or 1
  local w, h = self.w, self.h
  local y1 = y + 1
  local y2 = h
  local dy = 1
  if y < 0 then
    y1, y2 = y2, y1
    dy = -1
  end
  local x1 = x + 1
  local x2 = w
  local dx = 1
  if y < 0 then
    x1, x2 = x2, x1
    dx = -1
  end
  local _opti_canvas_get = self.get
  local _opti_canvas_set = self.set
  for j=y1,y2,dy do
    for i=x1,x2,dx do
      local packed = _opti_canvas_get(self, i, j)
      if packed then
        _opti_canvas_set(self, packed, i - x, j - y)
      end
    end
  end
  local _opti_canvas_drawFilledBox = self.drawFilledBox
  local clear = self.cbg
  if y > 0 then
    _opti_canvas_drawFilledBox(self, 1, h + 1 - y, w, h, clear)
  elseif y < 0 then
    _opti_canvas_drawFilledBox(self, 1, 1, w, -y, clear)
  end
  if x > 0 then
    _opti_canvas_drawFilledBox(self, w + 1 - x, 1, w, h, clear)
  elseif x < 0 then
    _opti_canvas_drawFilledBox(self, 1, 1, -x, h, clear)
  end
end

function sh.graphics.canvas:wrap(...)
  local current = term.current()
  term.redirect(self.term)
  local ret = { sh.graphics.write(...) }
  term.redirect(current)
  return table.unpack(ret)
end

function sh.graphics.canvas:print(...)
  local current = term.current()
  term.redirect(self.term)
  local ret = { sh.graphics.print(...) }
  term.redirect(current)
  return table.unpack(ret)
end

local function getDataFromPaintutilsColor(canvas, arg)
  if arg == nil then
    arg = canvas.cbg
  elseif arg == false then
    return nil
  end
  return sh.graphics.canvas.pack(space, canvas.cfg, arg)
end

function sh.graphics.canvas:drawPixel(x, y, color)
  local packed = getDataFromPaintutilsColor(self, color)
  self.cx = x + 1
  self.cy = y
  return self:set(packed, x, y)
end

function sh.graphics.canvas:drawLine(x1, y1, x2, y2, color)
  local _opti_canvas_set = self.set
  local packed = getDataFromPaintutilsColor(self, color)

  local dx = math.abs(x2 - x1)
  local dy = math.abs(y2 - y1)
  local sx = x1 < x2 and 1 or -1
  local sy = y1 < y2 and 1 or -1
  local err = dx - dy

  while true do
    _opti_canvas_set(self, packed, x1, y1)
    if x1 == x2 and y1 == y2 then break end
    local e2 = 2 * err
    if e2 > -dy then
      err = err - dy
      x1 = x1 + sx
    end
    if e2 < dx then
      err = err + dx
      y1 = y1 + sy
    end
  end
end

function sh.graphics.canvas:drawBox(x1, y1, x2, y2, color)
  local _opti_canvas_drawFilledBox = self.drawFilledBox
  _opti_canvas_drawFilledBox(self, x1, y1, x2, y1, color)
  _opti_canvas_drawFilledBox(self, x1, y1, x1, y2, color)
  _opti_canvas_drawFilledBox(self, x1, y2, x2, y2, color)
  _opti_canvas_drawFilledBox(self, x2, y1, x2, y2, color)
end

function sh.graphics.canvas:drawFilledBox(x1, y1, x2, y2, color)
  local packed = getDataFromPaintutilsColor(self, color)
  local _opti_canvas_set = self.set
  if x1 > x2 then x1, x2 = x2, x1 end
  if y1 > y2 then y1, y2 = y2, y1 end
  for x=x1,x2 do
    for y=y1,y2 do
      _opti_canvas_set(self, packed, x, y)
    end
  end
  self.cx = x2 + 1
  self.cy = y2
end

function sh.graphics.canvas:drawCanvas(x, y, canvas, options)
  x = x or self.cx
  y = y or self.cy
  canvas:output(self, x, y, options)
end

function sh.graphics.canvas:flush(options)
  self:output(self.parent, nil, nil, options)
end

function sh.graphics.canvas:flushUpstream(...)
  self:flush(...)
  if sh.class.of(self.parent, sh.graphics.canvas) then
    self.parent:flushUpstream(...)
  end
end

function sh.graphics.canvas:flushDownstream(...)
  for k,v in pairs(self.children) do
    v:flushDownstream(...)
  end
  self:flush(...)
end

function sh.graphics.canvas:setCursorBlink(...)
  return term.native().setCursorBlink(...)
end

function sh.graphics.canvas:isColor(...)
  return term.native().isColor(...)
end

--TODO move into trait like system
function sh.graphics.canvas:getPosition()
  return self.x, self.y
end

function sh.graphics.canvas:getSize()
  return self.w, self.h
end

function sh.graphics.canvas:getEndPosition()
  return self.x + self.w - 1, self.y + self.h - 1
end

function sh.graphics.canvas:getParentPos(x, y)
  local cx, cy = self:getCursorPos()
  local ox, oy = self:getPosition()
  x = x or cx
  y = y or cy
  return x + ox - 1, y + oy - 1
end

function sh.graphics.canvas:getScreenPosition()
  local window = self
  local sx, sy = 1, 1
  while window.proxy do
    local x, y = window.proxy.getPosition()
    sx = sx + x - 1
    sy = sy + y - 1
    window = window.proxy.parent
    window = window.parent
  end
  return sx, sy
end

function sh.graphics.canvas:getScreenEndPosition()
  local sx, sy = self:getScreenPosition()
  local w, h = self:getSize()
  return sx + w - 1, sy + h - 1
end

sh.graphics.canvas = sh.class.new(sh.graphics.canvas, 'Canvas Object')
