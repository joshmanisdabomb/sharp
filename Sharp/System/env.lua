sh.env = {}

sh.env.tostring = tostring
sh.env.pairs = pairs
sh.env.ipairs = ipairs

sh.env.print = print
sh.env.write = write
sh.env.error = error

sh.env.string = string
sh.env.table = table
sh.env.math = math

sh.env.os = { version = os.version, getComputerID = os.getComputerID, getComputerLabel = os.getComputerLabel, clock = os.clock, time = os.time, day = os.day }
sh.env.keys = keys
sh.env.textutils = sh.table.unset(sh.table.clone(textutils), 'slowPrint', 'slowWrite', 'pagedTabulate', 'pagedPrint')
sh.env.term = sh.table.replace(sh.table.clone(term), { native = function () return sh.instances.current.canvas.term end })
sh.env.colors = colors
sh.env.paintutils = sh.table.unset(sh.table.clone(paintutils), 'loadImage')

sh.env.sharp = sh.api

--Place for apps to insert their code.
sh.env.app = {}

sh.boot.log("Created application environment sandbox template.", colors.green)