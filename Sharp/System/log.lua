sh.log = {}

fs.makeDir(sh.path.logs)

function sh.log.create(entries, ...)
  local ret = { entries = {} }
  local callbacks = {...}

  function ret.write(text, color, severity)
    return ret.add({ text = text, color = color, severity = severity })
  end

  function ret.add(entry)
    if type(entry.text) == 'table' then
      local str = tostring(entry.text)
      local result = { pcall(textutils.serialise, entry.text) }
      if result[1] then
        entry.text = str .. result[2]
      end
    else
      entry.text = tostring(entry.text)
    end
    entry.severity = entry.severity or 0
    entry.origin = entry.origin or shell.getRunningProgram()
    entry.time = entry.time or sh.about.clock()
    table.insert(ret.entries, entry)
    entry.index = #ret.entries + 1
    for k,v in ipairs(callbacks) do
      v(entry, k)
    end
  end

  for k,v in ipairs(entries) do
    ret.add(v)
  end
  return ret
end

function sh.log.file(file)
  if fs.exists(file) then
    fs.delete(file)
  end
  local handle = fs.open(file, 'a')
  return function(entry, k)
    local severity = entry.severity >= 2 and 'ERROR' or (entry.severity >= 1 and 'WARNING' or 'INFO')
    local str = entry.time .. ' - ' .. severity .. ' in ' .. entry.origin .. ': ' .. tostring(entry.text)
    handle.writeLine(str)
    handle.flush()
  end
end

function sh.log.print()
  return sh.log.output
end

function sh.log.output(entry)
  term.setBackgroundColor(colors.black)
  term.setTextColor(colors.white)
  write(entry.time .. ' ')

  term.setTextColor(colors.black)
  if entry.severity >= 2 then
    term.setBackgroundColor(colors.red)
    write(' E ')
  elseif entry.severity >= 1 then
    term.setBackgroundColor(colors.orange)
    write(' W ')
  else
    term.setBackgroundColor(colors.cyan)
    write(' I ')
  end

  term.setBackgroundColor(colors.black)
  write(' ')

  term.setBackgroundColor(entry.color)
  if entry.color == colors.white or entry.color == colors.lightGray then
    term.setTextColor(colors.black)
  else
    term.setTextColor(colors.white)
  end
  write(' ' .. entry.origin .. ' ')

  term.setBackgroundColor(colors.black)
  term.setTextColor(colors.white)
  print(' ' .. tostring(entry.text))
end

local callbacks = {}
if boot.file then
  local path = fs.combine(sh.path.logs, 'boot.log')
  if type(boot.file) == 'string' then
    path = boot.file
  else
    table.insert(callbacks, sh.log.file(fs.combine(sh.path.logs, 'boot-' .. sh.about.session .. '.log')))
  end
  table.insert(callbacks, sh.log.file(path))
  sh.boot.log("Writing boot log to file " .. path, colors.purple)
end
if boot.print then
  table.insert(callbacks, sh.log.print())
  sh.boot.log('Printing boot log to screen.', colors.purple)
end
sh.log.boot = sh.log.create(boot.current, table.unpack(callbacks))
sh.log.boot.file = boot.file
sh.log.boot.print = boot.print
sh.boot.log("Successfully created boot log.", colors.green)

sh.log.runtime = sh.log.create({},
  sh.log.file(fs.combine(sh.path.logs, 'runtime.log')),
  sh.log.file(fs.combine(sh.path.logs, 'runtime-' .. sh.about.session .. '.log'))
)
sh.log.runtime.file = path
sh.boot.log("Successfully created runtime log.", colors.green)