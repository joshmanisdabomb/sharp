sh.about = {}
sh.about.name = "Sharp"

sh.about.major = 0
sh.about.minor = 7

sh.about.code = 'a'
sh.about.stage = "Alpha"

sh.about.short = sh.about.code .. sh.about.major .. "." .. sh.about.minor
sh.about.long = sh.about.name .. " " .. sh.about.stage .. " " .. sh.about.major .. "." .. sh.about.minor
sh.boot.log("Identified as " .. sh.about.long, colors.purple)

--Track how many times Sharp has been run.
sh.about.session = 1
local sessionPath = fs.combine(sh.path.data, 'session.dat')
if fs.exists(sessionPath) then
  local sessionHandle = fs.open(sessionPath, 'r')
  sh.about.session = tonumber(sessionHandle.readAll()) + 1
  sessionHandle.close()
end
local sessionHandle = fs.open(sessionPath, 'w')
sessionHandle.write(sh.about.session)
sessionHandle.close()
sh.boot.log("Boot session " .. sh.about.session .. '.', colors.purple)

sh.about.ran = sh.boot.time
function sh.about.clock()
  return os.clock() - sh.about.ran
end