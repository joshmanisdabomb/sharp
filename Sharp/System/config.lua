sh.config = {}

local system, reason = fs.open(fs.combine(sh.path.system, 'config.dat'), 'r')
if not system then
  sh.boot.error(reason)
end
sh.config.system = textutils.unserialise(system.readAll())
if not sh.config.system then
  sh.boot.error("Failed to unserialise system config.")
end

local data, reason = fs.open(fs.combine(sh.path.data, 'config.dat'), 'r')
if data then
  sh.config.data = textutils.unserialise(data.readAll())
  if not sh.config.data then
    sh.boot.error("Failed to unserialise data config.")
  end
else
  sh.config.data = {}
  sh.boot.log('config.dat in data folder does not exist, skipping.', colors.orange, 1)
end

sh.config.values = sh.table.clone(sh.config.system)
for k,v in pairs(sh.config.data) do
  sh.config.values[k] = v
end

setmetatable(sh.config, { __index = sh.config.values })