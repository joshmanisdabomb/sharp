sh.args = {}

function sh.args.read(args, booleans)
  booleans = booleans or {}
  local ret = { raw = args, arguments = {}, switches = {}, options = {}, all = {} }

  --Switch and option handling.
  local last = nil
  local option = false
  for i=1,#args,1 do
    for k,v in pairs(booleans) do
      if last == v then
        last = nil
      end
    end
    if string.sub(args[i], 1, 1) == '-' then
      if string.sub(args[i], 1, 2) == '--' then
        last = string.sub(args[i], 3, -1)
        ret.options[last] = true
        option = true
      else
        local after = string.sub(args[i], 2, -1)
        for char in string.gmatch(after, '.') do
          last = char
          ret.switches[last] = true
        end
        option = false
      end
    elseif last then
      if option then
        ret.options[last] = args[i]
      else
        ret.switches[last] = args[i]
      end
      last = nil
    else
      table.insert(ret.arguments, args[i])
    end
  end

  for k,v in ipairs(ret.arguments) do
    ret.all[k] = v
  end
  for k,v in pairs(ret.switches) do
    ret.all[k] = v
  end
  for k,v in pairs(ret.options) do
    ret.all[k] = v
  end

  return ret
end

sh.args.boot = sh.args.read(args)