sh.path = {}

--Boot file path. (Usually Sharp/System/boot.lua)
sh.path.boot = boot
sh.boot.log("Set boot file path to " .. sh.path.boot, colors.purple)

--Wrapper file path. (Usually startup.lua)
if sh.wrapper then
  sh.path.wrapper = sh.wrapper.path
  sh.boot.log("Set wrapper path to " .. sh.path.wrapper, colors.purple)
else
  sh.boot.log("No wrapper path set, Sharp may not have been loaded with a wrapper script.", colors.orange, 1)
end

--System path. (Usually Sharp/System)
sh.path.system = fs.combine(sh.path.boot, "..")
sh.boot.log("Set system path to " .. sh.path.system, colors.purple)

--Sharp path. (Usually Sharp)
sh.path.sharp = fs.combine(sh.path.system, "..")
sh.boot.log("Set application path to " .. sh.path.sharp, colors.purple)

--Logs path. (Usually Sharp/Logs)
sh.path.logs = fs.combine(sh.path.sharp, "Logs")
sh.boot.log("Set logs path to " .. sh.path.logs, colors.purple)

--Apps path. (Usually Sharp/Apps)
sh.path.apps = fs.combine(sh.path.sharp, "Apps")
sh.boot.log("Set apps path to " .. sh.path.apps, colors.purple)

--Users path. (Usually Sharp/Users)
sh.path.users = fs.combine(sh.path.sharp, "Users")
sh.boot.log("Set users path to " .. sh.path.users, colors.purple)

--Data path. (Usually Sharp/Data)
sh.path.data = fs.combine(sh.path.sharp, "Data")
sh.boot.log("Set data path to " .. sh.path.apps, colors.purple)