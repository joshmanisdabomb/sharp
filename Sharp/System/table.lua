sh.table = {}

function sh.table.hash(tbl)
  return tostring(tbl)
end

function sh.table.contains(tbl, value)
  for k,v in pairs(tbl) do
    if v == value then
      return true, k
    end
  end
  return false
end

--Credit to https://stackoverflow.com/a/26367080
function sh.table.clone(tbl, seen)
  if type(tbl) ~= 'table' then return tbl end
  if seen and seen[tbl] then return seen[tbl] end
  local s = seen or {}
  local res = setmetatable({}, getmetatable(tbl))
  s[tbl] = res
  for k, v in pairs(tbl) do res[sh.table.clone(k, s)] = sh.table.clone(v, s) end
  return res
end

function sh.table.copy(tbl)
  local new = {}
  for k,v in pairs(tbl) do
    new[k] = v
  end
  return new
end

function sh.table.replace(tbl, ...)
  for k,v in ipairs({...}) do
    for k2,v2 in pairs(v) do
      tbl[k2] = v2
    end
  end
  return tbl
end

function sh.table.merge(tbl, ...)
  for k,v in ipairs({...}) do
    for k2,v2 in pairs(v) do
      if type(k2) == "string" then
        tbl[k2] = v2
      else
        table.insert(tbl, v2)
      end
    end
  end
  return tbl
end

function sh.table.default(tbl, ...)
  for k,v in ipairs({...}) do
    for k2,v2 in pairs(v) do
      if tbl[k2] == nil then
        tbl[k2] = v2
      end
    end
  end
  return tbl
end

function sh.table.unset(tbl, ...)
  for k,v in ipairs({...}) do
    tbl[v] = nil
  end
  return tbl
end

function sh.table.keys(tbl)
  local keys = {}
  for k,v in pairs(tbl) do
    table.insert(keys, k)
  end
  return keys
end

function sh.table.values(tbl)
  local values = {}
  for k,v in pairs(tbl) do
    table.insert(values, v)
  end
  return values
end

function sh.table.map(tbl, transform)
  for k,v in pairs(tbl) do
    local new, index = transform(v, k)
    tbl[index or k] = new
    if index ~= nil and index ~= k then
      tbl[k] = nil
    end
  end
  return tbl
end

function sh.table.range(from, to, step)
  local ret = {}
  from = from or 1
  step = step or 1
  for i=from,to,step do
    ret[#ret + 1] = i
  end
  return ret
end

local function sortedIterator(tbl, keys, cmp)
  cmp = cmp or function(a, b) return a < b end
  table.sort(keys, function(a, b) return cmp(tbl[a], tbl[b], a, b) end)

  local i = 0
  return function()
    i = i + 1
    if keys[i] == nil then
      return nil
    else
      return keys[i], tbl[keys[i]], i
    end
  end, keys
end

function sh.table.pairsSorted(tbl, cmp)
  return sortedIterator(tbl, sh.table.keys(tbl), cmp)
end

function sh.table.ipairsSorted(tbl, cmp)
  sh.log.runtime.write(sh.table.range(1, #tbl))
  return sortedIterator(tbl, sh.table.range(1, #tbl), cmp)
end

function sh.table.rep(tbl, count)
  if type(tbl) ~= "table" then tbl = {tbl} end
  local source = sh.table.clone(tbl)
  for i=2,count,1 do
    tbl = sh.table.merge(tbl, source)
  end
  return tbl
end

function sh.table.fill(value, count)
  return sh.table.rep({value}, count)
end

function sh.table.reverse(tbl)
  local mid = math.floor(#tbl/2)
  for i=1,mid,1 do
    tbl[i], tbl[#tbl-i+1] = tbl[#tbl-i+1], tbl[i]
  end
  return tbl
end

-- Credit to https://stackoverflow.com/a/8695525
function sh.table.reduce(tbl, fn, init)
  local acc = init
  for k, v in ipairs(tbl) do
    if 1 == k and not init then
      acc = v
    else
      acc = fn(acc, v, k)
    end
  end
  return acc
end

function sh.table.implode(tbl, separator)
  return sh.table.reduce(tbl, function(acc, el, k)
    return acc .. tostring(el) .. (k == #tbl and '' or separator)
  end, '')
end

function sh.table.explode(str, separator)
  local tbl = {}
  for token in string.gmatch(str, "([^" .. separator .. "]+)") do
    table.insert(tbl, token)
  end
  return tbl
end

function sh.table.compare(...)
  local tables = {...}

  local function recursive(o1, o2, p1, p2)
    if o1 == o2 then
      return true, "=="
    end

    if type(o1) ~= "table" or type(o2) ~= "table" then
      return false, "~=", p1, p2
    end

    local mt = getmetatable(o1)
    if mt and mt.__eq then
      return false, "metatable.__eq", p1, p2
    end

    for k,v in pairs(o1) do
      local result = { recursive(v, o2[k], p1 .. '.' .. k, p2 .. '.' .. k) }
      if not result[1] then
        return table.unpack(result)
      end
    end

    return true, "deep"
  end

  local result = { true, "==" }
  for k,v in ipairs(tables) do
    for k2,v2 in ipairs(tables) do
      if k ~= k2 then
        result = { recursive(v, v2, tostring(k), tostring(k2)) }
        if not result[1] then
          return table.unpack(result)
        end
      end
    end
  end
  return table.unpack(result)
end

function sh.table.get(tbl, ...)
  local paths = {...}
  local ret = {}
  for i=1,math.max(#paths,1),1 do
    local path = paths[i]
    if tostring(path) == '' then
      ret[i] = tbl
    elseif type(path) ~= 'string' then
      ret[i] = tbl[path]
    else
      ret[i] = tbl
      for part in string.gmatch(path, "[^.]+") do
        ret[i] = ret[i][part]
      end
    end
  end
  return table.unpack(ret)
end

function sh.table.extract(tbl, path)
  return sh.table.map(sh.table.copy(tbl), function(tbl2) return sh.table.get(tbl2, path) end)
end

function sh.table.chunk(tbl, size)
  local ret = {}
  for i=1,#tbl do
    local index = math.ceil(i / size)
    if not ret[index] then ret[index] = {} end
    table.insert(ret[index], tbl[i])
  end
  return ret
end

function sh.table.transpose(tbl)
  local ret = {}
  for k,v in pairs(tbl) do
    for k2,v2 in pairs(v) do
      if not ret[k2] then ret[k2] = {} end
      ret[k2][k] = v2
    end
  end
  return ret
end