sh.math = {}

function sh.math.clamp(num, lb, ub)
  return math.min(math.max(num, lb), ub)
end