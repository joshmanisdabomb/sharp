sh.graphics = {}
sh.graphics.width,sh.graphics.height = term.getSize()
sh.graphics.main = nil
sh.graphics.window = nil

function sh.graphics.init(...)
  sh.graphics.window = window.create(term.current(), 1, 1, sh.graphics.width, sh.graphics.height, false)
  sh.graphics.main = sh.graphics.canvas({}, sh.graphics.window, ...)
  return sh.graphics.main
end

function sh.graphics.alignCenter(text, x)
  x = x or term.getCursorPos()
  return math.ceil(x)-math.ceil((string.len(text)-2)/2)
end

function sh.graphics.alignCenterAbsolute(text)
  return sh.graphics.alignCenter(text, math.ceil(term.getSize() / 2))
end

function sh.graphics.alignRight(text, x)
  x = x or term.getCursorPos()
  return x + 1 - string.len(text)
end

function sh.graphics.alignRightAbsolute(text)
  return sh.graphics.alignRight(text, (term.getSize()))
end

function sh.graphics.text(text, options)
  options = options or {}
  local tx, ty = term.getCursorPos()
  local x = options.x or tx
  local y = options.y or ty
  local tw, th = term.getSize()
  local w = options.w or tw
  local h = options.h or th
  local writer = options.writer or term.write
  local align = options.align or function (text, x) return x end
  local scroll = options.scroll or term.scroll
  local newX = options.newlineX or x

  if type(text) == 'table' then
    text = sh.table.implode(text, '\t')
  else
    text = tostring(text)
  end

  local lines = sh.string.wrap(text, math.max(w + 1 - x, 1))
  if #lines > 1 then
    local first = lines[1]
    local after = sh.string.ltrim(string.sub(text, string.len(first) + 1))
    if string.byte(after) == 10 then
      after = string.sub(after, 2)
    end
    local next = sh.string.wrap(after, math.max(w + 1 - newX, 1))
    lines = sh.table.merge({ first }, next)
  end

  local amount = #lines
  if scroll then
    local scrolls = (y + amount - 1) - h
    if scrolls > 0 then
      y = y - scrolls
      scroll(scrolls)
    end
  end

  term.setCursorPos(align(lines[1], x), y)
  for i=1,amount,1 do
    local line = lines[i]
    writer(line)
    if i < amount then
      term.setCursorPos(align(lines[i + 1], newX), y + i)
    end
  end

  return lines
end

function sh.graphics.print(text, ...)
  if type(text) == 'table' then
    text[#text] = text[#text] .. '\n'
  else
    text = text .. '\n'
  end
  return sh.graphics.text(text, ...)
end

function sh.graphics.writeCenter(text, x)
  local func = 'alignCenter'
  if x == true then
    func = func .. 'Absolute'
  end
  local x = sh.graphics[func](text, x)
  sh.graphics.setCursorX(x)
  term.write(text)

  return x
end

function sh.graphics.writeRight(text, x)
  local func = 'alignRight'
  if x == true then
    func = func .. 'Absolute'
  end
  local x = sh.graphics[func](text, x)
  sh.graphics.setCursorX(x)
  term.write(text)

  return x
end

function sh.graphics.setCursorX(x)
  local _,y = term.getCursorPos()
  term.setCursorPos(x, y)
end

function sh.graphics.setCursorY(y)
  local x = term.getCursorPos()
  term.setCursorPos(x, y)
end

function sh.graphics.flash(window)
  local visible = window.isVisible()
  window.setVisible(true)
  window.redraw()
  window.setVisible(visible)
end
