sh.routine = {}

sh.routine.ups = 20
sh.routine.updateRate = 1/sh.routine.ups
sh.routine.tick = 0
sh.routine.tickMod = 0

function sh.routine.each(...)
  local layers = { sh.instances.system }
  sh.table.merge(layers, sh.table.map(sh.users.active, function(u) return u.layer end))

  for k,v in pairs(layers) do
    local entries = sh.table.copy(v.entries)
    table.sort(entries, function (a, b)
      if (a.instance.app.priority ~= b.instance.app.priority) then
        return a.instance.app.priority < b.instance.app.priority
      end
      if (a.foreground ~= b.foreground) then
        return a.foreground < b.foreground
      end
      return a.focus < b.focus
    end)

    for k,v in ipairs({...}) do
      for k2,v2 in pairs(entries) do
        if v(v2) then
          return layers
        end
      end
    end
  end

  return layers
end

function sh.routine.update()
  while true do
    if not sh.routine.pause then
      sh.routine.each(function (entry)
        if entry.foreground then
          if entry.focus <= 1 then
            sh.err.handler(entry.instance.routine.active, sh.err.appRun, entry.instance, 'active')
          end
          sh.err.handler(entry.instance.routine.foreground, sh.err.appRun, entry.instance, 'foreground')
        end
      end, function (entry)
        sh.err.handler(entry.instance.routine.background, sh.err.appRun, entry.instance, 'background')
      end)

      sh.graphics.main:flush()
      sh.graphics.flash(sh.graphics.window)
      sleep(sh.routine.updateRate)

      sh.routine.tick = sh.routine.tick + 1
      sh.routine.tickMod = sh.routine.tick % sh.routine.ups
    else
      sleep(sh.routine.updateRate)
    end
    --sh.log.draw()
  end
end

function sh.routine.event()
  while true do
    if not sh.routine.pause then
      local event = { os.pullEventRaw() }

      sh.mouse.handle(event)

      sh.routine.each(function (entry)
        local ok, cancel = sh.err.handler(function() return entry.instance.routine.handle(event) end, sh.err.appRun, entry.instance, 'handle')
        if ok then
          return cancel
        end
      end)

      sh.graphics.main:flush()
      sh.graphics.flash(sh.graphics.window)
    else
      sleep(sh.routine.updateRate)
    end
    --sh.log.draw()
  end
end

function sh.routine.start()
  term.setBackgroundColor(colors.black)
  term.setTextColor(colors.white)
  term.clear()
  term.setCursorPos(1,1)
  term.setCursorBlink(false)

  sh.routine.coroutines = sh.table.map({ sh.routine.update, sh.routine.event }, function(f) return table.pack(coroutine.create(f))[1] end)
  
  local filters = {}
  local event = {}
  local ret = nil
  while not ret do
    for i=1,#sh.routine.coroutines,1 do
      sh.routine.coroutine = sh.routine.coroutines[i]
      if filters[sh.routine.coroutine] == nil or filters[sh.routine.coroutine] == event[1] or event[1] == "terminate" then
        local timer = os.clock()
        local ok, result = coroutine.resume(sh.routine.coroutine, table.unpack(event))
        timer = os.clock() - timer
        if timer > sh.routine.updateRate then
          sh.log.runtime.write('Tick ' .. sh.routine.tick .. ' took ' .. timer .. 's to process, greater than target time ' .. sh.routine.updateRate .. 's.')
        end
        if not ok then
          error(result, 0)
        else
          filters[sh.routine.coroutine] = result
        end
        if sh.routine.pause then
          break
        end
        if coroutine.status(sh.routine.coroutine) == "dead" then
          ret = sh.routine.coroutine
          break
        end
      end
    end
    event = { os.pullEventRaw() }
  end
  error("Parallel function " .. ret .. " in routine unexpectedly terminated.")
end

sh.boot.log("Starting routine code.", colors.lime)