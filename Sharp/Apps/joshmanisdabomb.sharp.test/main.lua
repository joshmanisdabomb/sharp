---@diagnostic disable: undefined-global

--This function allows you to set metadata for the application, runs once when Sharp loads the app.
function app.settings()
  return {
    name = "Test",
    priority = 0
  }
end

local function drawBox(instance)
  local x1 = math.random(1, instance.canvas.width)
  local x2 = math.random(1, instance.canvas.width)
  local y1 = math.random(1, instance.canvas.height)
  local y2 = math.random(1, instance.canvas.height)
  instance.canvas:drawBox(x1, y1, x2, y2, 2^math.random(1,15))
end

local function clear(instance)
  instance.canvas:setBackgroundColor(colors.white)
  instance.canvas:clear()
end

--This function allows you to do anything when Sharp is booting.
function app.startup()
end

--This function runs when the app is loaded into the background.
function app.start(instance)
end

--This function runs when the app is restored into the foreground.
function app.open(instance)
end

--This function runs every tick when the app is currently in focus.
function app.active(instance)
end

--This function runs when the app is given focus.
function app.focus(instance)
end

--This function runs when the app has lost focus.
function app.blur(instance)
end

--This function runs every tick when the app is running in the foreground.
function app.foreground(instance)
  if instance.fg then
    clear(instance)
    drawBox(instance)
    instance.canvas:flush()
  end
end

--This function runs every tick, even when the app is running in the background.
function app.background(instance)
end

--This function runs when an event is fired, even when the app is running in the background. Return true to not allow instances with lower priority and focus to process this event.
function app.handle(instance, event)
  if event[1] == 'key' then
    local flush = false
    if event[2] == keys.space then
      drawBox(instance)
      flush = true
    elseif event[2] == keys.enter then
      clear(instance)
      flush = true
    elseif event[2] == keys.a then
      instance.fg = not instance.fg
      flush = true
    end
    if flush then
      instance.canvas:flush()
    end
  end
end

--This function runs when the app is minimised into the background.
function app.close(instance)
end

--This function runs when the app is removed from the background.
function app.destroy(instance)
end