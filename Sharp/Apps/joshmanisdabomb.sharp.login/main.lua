---@diagnostic disable: undefined-global

local messages = {
  begin = 'To begin, click your username.',
  welcome = 'Welcome back %s.',
}
local paneUserW = 20

--This function allows you to set metadata for the application, runs once when Sharp loads the app.
function app.settings()
  return {
    name = "Login",
    priority = 0
  }
end

--This function allows you to do anything when Sharp is booting.
function app.startup()
end

--This function runs when the app is loaded into the background.
function app.start(instance)
  instance.ui = sharp.ui.container({ scroll = false, clear = colors.black }, instance.window)
  
  local paneUsers = instance.ui:child(sharp.ui.container, { padding = { x = 1 }, clear = colors.black })
  local title = paneUsers:child(sharp.ui.text, { text = messages.begin, clear = colors.black, align = 'center', padding = { y = 1 } })
  local cards = paneUsers:child(sharp.ui.container, { scroll = { y = true }, clear = colors.black })

  local paneUser = instance.ui:child(sharp.ui.container, { z = -1, clear = colors.lightGray, padding = 1 }, sharp.graphics.width - 9, 1, 0, instance.ui.h)
  local username = paneUser:child(sharp.ui.text, { text = 'Username', clear = colors.lightGray, color = colors.black, align = 'left' }, nil, nil, paneUserW)

  for k,v in pairs(sharp.table.values(sharp.users.all)) do
    local card = cards:child(sharp.ui.card, { title = { text = v.name, padding = { x = 1, y = 1, l = 2 } } }, 1, 1, sharp.graphics.width - 2, 3)

    function card:drawAfter()
      local w, h = term.getSize()
      paintutils.drawLine(1, 1, 1, h, v.active and colors.lime or colors.gray)
    end

    function card:act(event)
      username.text = v.name
      if paneUser.w == 0 then
        title.text = string.format(messages.welcome, card.title.text)
        card.active = true
        paneUser:animate(sharp.ui.animations.reposition(paneUser, nil, nil, paneUserW, nil, sharp.ui.animations.variable(0.2, function (prev) return prev * 1.5 end)), { group = 1 })
      elseif paneUser.w == paneUserW then
        title.text = messages.begin
        card.active = false
        paneUser:animate(sharp.ui.animations.reposition(paneUser, nil, nil, 0, nil, sharp.ui.animations.variable(1, function (prev) return prev * 1.4 end)), { group = 1 })
      end
    end

    card:redraw()
  end

  paneUsers:arrange({{{ element = title, width = 'fill', height = 'hug' }}, {{ element = cards, width = 'fill', height = 'fill' }}})
  cards:arrange('vfstack')
  paneUser:arrange({{{ element = username, width = 'fixed', height = 'hug' }}})

  instance.ui:arrange({
    {{ element = paneUsers, width = 'fill', height = 'fill' }, { element = paneUser, width = 'fixed', height = 'fill' }}
  })
  instance.ui:redraw()
end

--This function runs when the app is restored into the foreground.
function app.open(instance)
  instance.window.setBackgroundColor(colors.black)
  instance.window.clear()

  sharp.ui.draw(instance.ui)
end

--This function runs every tick when the app is currently in focus.
function app.active(instance)
end

--This function runs when the app is given focus.
function app.focus(instance)
end

--This function runs when the app has lost focus.
function app.blur(instance)
end

--This function runs every tick when the app is running in the foreground.
function app.foreground(instance)
  sharp.ui.foreground(instance.ui)
end

--This function runs every tick, even when the app is running in the background.
function app.background(instance)
end

--This function runs when an event is fired, even when the app is running in the background. Return true to not allow instances with lower priority and focus to process this event.
function app.handle(instance, event)
  if sharp.ui.handle(event, instance.ui) then
    return true
  end
end

--This function runs when the app is minimised into the background.
function app.close(instance)
end

--This function runs when the app is removed from the background.
function app.destroy(instance)
end