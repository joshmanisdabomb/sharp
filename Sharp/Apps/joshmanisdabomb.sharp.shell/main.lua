---@diagnostic disable: undefined-global

--This function allows you to set metadata for the application, runs once when Sharp loads the app.
function app.settings()
  return {
    name = "Sharp UI",
    priority = 1
  }
end

--This function allows you to do anything when Sharp is booting.
function app.startup()
end

--This function runs when the app is loaded into the background.
function app.start(instance)
  instance.window.scissors[1] = { x1 = 1, y1 = 2, x2 = sharp.graphics.width, y2 = sharp.graphics.height - 1 }
end

--This function runs when the app is restored into the foreground.
function app.open(instance)
  term.setCursorPos(1, 1)
  term.setBackgroundColor(colors.gray)
  term.setTextColor(colors.lightGray)
  term.clearLine()
  sharp.graphics.centerWrite("---")

  term.setCursorPos(1, sharp.graphics.height)
  term.clearLine()
end

--This function runs every tick when the app is currently in focus.
function app.active(instance)
end

--This function runs when the app is given focus.
function app.focus(instance)
end

--This function runs when the app has lost focus.
function app.blur(instance)
end

--This function runs every tick when the app is running in the foreground.
function app.foreground(instance)
  term.setTextColor(colors.white)

  term.setCursorPos(1, 1)
  term.write(sharp.about.long)

  term.setCursorPos(sharp.graphics.width, 1)
  sharp.graphics.rightWrite(sharp.string.lpad(textutils.formatTime(os.time()), ' ', 8))
end

--This function runs every tick, even when the app is running in the background.
function app.background(instance)
end

--This function runs when an event is fired, even when the app is running in the background. Return true to not allow instances with lower priority and focus to process this event.
function app.handle(instance, event)
  if event[1] == "mouse_click" then
    if event[4] == instance.y then
      error("Nav bar " .. instance.id)
    end
  end
end

--This function runs when the app is minimised into the background.
function app.close(instance)
end

--This function runs when the app is removed from the background.
function app.destroy(instance)
end